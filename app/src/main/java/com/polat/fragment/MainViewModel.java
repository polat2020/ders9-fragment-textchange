package com.polat.fragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {
    private MutableLiveData<String> nameInput = new MutableLiveData<>();

    public void setName(String name) {
        nameInput.setValue(name);
    }

    public LiveData<String> getName() {
        return nameInput;
    }
}
