package com.polat.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.polat.fragment.Fragment.FirstFragment;
import com.polat.fragment.Fragment.ThirdFragment;

public class MainActivity extends AppCompatActivity {

    MainViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        FragmentTransaction transaction= getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout1,
                FirstFragment.newInstance(),"tag1");


        transaction.add(R.id.frameLayout2,
                ThirdFragment.newInstance(),"tag2");

        transaction.commit();
    }
}